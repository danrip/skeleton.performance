﻿using System;

namespace NServiceBus.Skeleton.Performance
{
    public class DoStuffMessage : IMessage
    {
        public Guid Id { get; set; }
        public string MessageNumber { get; set; }
    }
}


using System.Globalization;
using log4net.Config;

namespace NServiceBus.Skeleton.Performance
{
    using NServiceBus;

	public class EndpointConfig : IConfigureThisEndpoint, AsA_Server, IWantToRunWhenBusStartsAndStops, IWantCustomLogging
	{
        public IBus Bus { get; set; }

	    public void Start()
	    {
	        for (int i = 0; i < 5000; i++)
	        {
	            Bus.SendLocal(new DoStuffMessage() {MessageNumber = i.ToString(CultureInfo.InvariantCulture)});
	        }
	    }

	    public void Stop()
	    {
	    }

	    public void Init()
	    {
            SetLoggingLibrary.Log4Net(() => XmlConfigurator.Configure());
	    }
	}
}

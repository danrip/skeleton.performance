﻿using log4net;

namespace NServiceBus.Skeleton.Performance
{
    public class MessageHandler : IHandleMessages<DoStuffMessage>
    {
        private static readonly ILog Logger = LogManager.GetLogger("NServicebus.Skeleton");
        public void Handle(DoStuffMessage message)
        {
            Logger.DebugFormat("Handling message nbr : {0}", message.MessageNumber);
        }
    }
}